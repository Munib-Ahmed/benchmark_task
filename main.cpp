#include <iostream>
#include <memory>
#include <vector>
#include <list>
#include <cstdlib>
#include <ctime>
#include <numeric>
#include "benchmark/benchmark.h"
using namespace std;

void sum_of_vector_squared(int num_entries)
{
  vector<uint16_t> vector_values;
  vector<uint16_t> vector_squared_values;
  srand(time(0));
    for (int i = 0; i < num_entries; i++)
    {
      vector_values.push_back(rand() % 100);
    }

    for (auto i = vector_values.begin(); i != vector_values.end(); ++i)
    {
      vector_squared_values.push_back((*i) * (*i));
    }
    uint16_t sum_of_elems = std::accumulate(vector_squared_values.begin(), vector_squared_values.end(), decltype(vector_squared_values)::value_type(0));
}

void sum_of_list_squared(int num_entries)
{
  list<uint16_t> list_values;
  list<uint16_t> list_squared_values;
  srand(time(0));
    for (int i = 0; i < num_entries; i++)
    {
      list_values.push_back(rand() % 100);
    }

    for (auto i = list_values.begin(); i != list_values.end(); ++i)
    {
      list_squared_values.push_back((*i) * (*i));
    }
    uint16_t sum_of_elems = std::accumulate(list_squared_values.begin(), list_squared_values.end(), decltype(list_squared_values)::value_type(0));
}


static void vector_benchmark(benchmark::State &state)
{ 
  for (auto _ : state)
  {
    for (int i=0; i<100; i++)
{
    sum_of_vector_squared(state.range(0));
  }
}
}
BENCHMARK(vector_benchmark)->Arg(5000);

static void list_benchmark(benchmark::State &state)
{
  for (auto _ : state)
  {
    for (int i=0; i<100; i++)
{
    sum_of_list_squared(state.range(0));
  }
}
}
BENCHMARK(list_benchmark)->Arg(5000);



BENCHMARK_MAIN();
/*
auto BM_test = [](benchmark::State& st, auto Inputs) {sum_of_list_squared,sum_of_vector_squared};

int main(int argc, char** argv) {
  for (auto& test_input : {sum_of_list_squared, sum_of_vector_squared})
      benchmark::RegisterBenchmark(test_input.name(), BM_test, test_input);
  benchmark::Initialize(&argc, argv);
  benchmark::RunSpecifiedBenchmarks();
  benchmark::Shutdown();
  return 0;
}*/